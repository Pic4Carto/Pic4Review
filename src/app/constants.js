export default {
	LEAFLET_IMG_PATH: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/',
	TILE_ATTRIBUTION: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
	TILE_URL: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
	ID_URL: 'https://www.openstreetmap.org/edit',
	JOSM_URL: 'http://127.0.0.1:8111/load_and_zoom?',
	OAPI_URL: 'https://overpass-api.de/api/interpreter',
	OSMOSE_API: 'https://osmose.openstreetmap.fr/api/0.3',

	// Production config
	OSM_API_URL: 'https://www.openstreetmap.org',
	OAUTH_CONSUMER_KEY: 'WofjNAUxebyuS40aRy5nxXQot1GEMDVy85-MfuP_wxk',
	P4R_URL: 'http://localhost:28113',
};
